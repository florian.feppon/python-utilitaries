import pickle
import dill

def save_pickle(obj, filename):
    with open(filename, 'wb') as outp:  # Overwrites any existing file.
        pickle.dump(obj, outp, pickle.HIGHEST_PROTOCOL)

def load_pickle(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)


def save_dill(obj, filename):
    with open(filename, 'wb') as outp:  # Overwrites any existing file.
        dill.dump(obj, outp)    

def load_dill(filename):
    with open(filename, 'rb') as f:
        return dill.load(f)
