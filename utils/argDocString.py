import sys
from utils import colored, display
import os
import sys
import re
import textwrap

class argDict(dict):
    """ Note: when using this object, it is customary to call the output
    "args".
    However this may conflict with the "args" command of the IPython or Python
    debugger (do print(args) in order to show the value of args)"""
    nargs = 0

    def copy(self):
        return argDict(list(zip(self.keys(),self.values())))

    def getdefault(self, key, value):
        if not key in self or not self[key]:
            return value
        else:
            if isinstance(key,int):
                if value is None:
                    return self[key]
                else:
                    return type(value)(self[key])
            if value is None:
                return self[key][0]
            else:
                return type(value)(self[key][0])

    def popdefault(self, key, value):
        if not key in self or not self[key]:
            return value
        else:
            if value is None:
                return self[key][0]
            else:
                return type(value)(self.pop(key)[0])

    def getpresent(self, key):
        if key in self:
            return True
        else:
            return False

    def poppresent(self, key):
        if key in self:
            self.pop(key)
            return True
        else:
            return False


def parseArgsToDict(args):
    if isinstance(args,str):
        args = args.split(" ")
    res = argDict()
    if not args:
        return res
    argsCpy = list(reversed(args))
    key = argsCpy.pop()
    i = 0
    pattern = re.compile('^--?[a-zA-Z]')
    while True:
        if pattern.match(key): 
            res[key] = []
            break
        res[i]=key
        i = i+1
        if argsCpy:
            key = argsCpy.pop()
        else:
            break
    res.nargs = i
    while argsCpy:
        token = argsCpy.pop()
        if pattern.match(token):
            key = token
            res[key] = []
        else:
            res[key].append(token)

    return res

def dictToArgs(args):
    command = ""
    for key in args:
        if isinstance(key,int):
            command += args[key] + " "
        else:
            command += key + " " + " ".join(args[key]) 
            if args[key]:
                command += " "
    command = command[:-1]
    return command

class argDocString:
    def __init__(self, description):
        self.command = os.path.split(sys.argv[0])[1]
        self.description = description
        self.args = dict()
        self.argsOptions = dict()
        self.nargs = 0
        self.nargsImportant = 0
        self.nargsNonImportant = 0
        self.altKeys = dict()

    def addArg(self,arg,description,meta='',alt=[],important=False,default=None):
        if isinstance(arg,int) and meta=='':
            raise Exception("Error: argument "+str(arg)+" requires a meta "
                            "description")
        if isinstance(arg,int):
            self.nargs += 1
            self.args[arg] = dict(description=description,
                                  meta=meta,
                                  alt=alt,
                                  default=default)
        else:
            self.argsOptions[arg] = dict(description=description,
                                         meta=meta,
                                         alt=alt,
                                         important=important,
                                         default=default)
            if important:
                self.nargsImportant += 1
            else:
                self.nargsNonImportant +=1 
        for key in alt:
            self.altKeys[key] = arg

    def docstring(self):
        docstring="\n"+self.command
        if not '-h' in self.argsOptions:
            self.addArg('-h','Display this help.\n',alt=['--help'])
        i=0
        while True:
            if i in self.args:
                docstring += " "+self.args[i]['meta']
                i += 1
            else:
                break
        nargs = i
        if len(self.argsOptions) >= 1:
            countNonImportant = 0
            countImportant = 0
            for key in self.argsOptions.keys():
                if self.argsOptions[key]['important']:
                    docstring += " ["
                    docstring += " "+key
                    if self.argsOptions[key]['meta']:
                        docstring += " "+self.argsOptions[key]['meta']
                    docstring += " ]"
                    countImportant += 1
                else:
                    countNonImportant+=1
            if countNonImportant:
                docstring += " OPTIONS"
        docstring = colored(docstring,attr="bold")
        docstring+="\n\n"
        docstring += self.description
        if len(self.args)>0 or countImportant:
            docstring += "\n"
            docstring += colored("\nARGUMENTS\n",attr="bold")
            docstring += "-"*10
            for i in range(nargs):
                docstring += "\n"+" "*2+colored(self.args[i]['meta'],attr="bold")
                if 'default' in self.args[i] and not self.args[i]['default'] is None:
                    docstring += "(default "+self.args[i]['meta']+"="+str(self.args[i]['default'])+")"
                docstring += "\n"
                docstring += textwrap.indent("\n".join(textwrap.wrap(self.args[i]['description'], width=75))," "*4)
                docstring += "\n"
            for key, arg in self.argsOptions.items():
                if arg['important']:
                    keys = key
                    if arg['alt']:
                        keys+=", "+", ".join(arg['alt'])
                    docstring += "\n"+" "*2+keys+" "+arg['meta']
                    if 'default' in arg and not arg['default'] is None:
                        docstring += " (default "+arg['meta']+"="+str(arg['default'])+")"
                    docstring += "\n"
                    docstring += textwrap.indent("\n".join(textwrap.wrap(arg['description'], width=75))," "*4)
                    docstring += "\n"

        if countNonImportant:
            docstring+="\n\n"
            docstring+=colored("OPTIONAL ARGUMENTS\n",attr="bold")
            docstring+="-"*len("OPTIONAL ARGUMENTS")
            for key, arg in self.argsOptions.items():
                keys = key
                if arg['alt']:
                    keys+=", "+", ".join(arg['alt'])
                if not arg['important']:
                    docstring += "\n"+" "*2+colored(keys,attr="bold")+" "+arg['meta']
                    if 'default' in arg and not arg['default'] is None:
                        docstring += " (default "+arg['meta']+"="+str(arg['default'])+")"
                    docstring += "\n"
                    docstring += textwrap.indent("\n".join(textwrap.wrap(arg['description'], width=75))," "*4)
                    docstring += "\n"
        return docstring

    def parseSysArgs(self, printHelp=True, severity=2):
        """
        severity : 0 (do not check arguments)
        severity : 1 (do not check non existing command options but 
                      raise an error if non conforming arguments)
        severity : 2 (raise errors and print warning for non existing options)
        """
        args = parseSysArgs()

        if ( '-h' in args or '--help' in args ):
            if printHelp:
                print(self.docstring())
                sys.exit(0)
            else:
                severity=0

        if severity>0:
            i=0
            while True:
                if i in args:
                    i += 1
                else:
                    break
            if i!= self.nargs and severity >= 1:
                display("Error, the command "+self.command+" expects "
                                f"{self.nargs} main argument" + ("s" if self.nargs > 1 else "") \
                                +": "+", ".join([value['meta'] for key, value in self.args.items()]) +\
                                ". See the documentation " 
                                "with -h", color="red")
                sys.exit(1)
            for key in list(args.keys()):
                if key in self.altKeys:
                    truekey = self.altKeys[key]
                    args[truekey] = args[key]
                else:
                    truekey = key
                if not isinstance(truekey,int):
                    if not truekey in self.argsOptions and severity>=2:
                        display("Error, the option "+key+" passed to the "
                                "command line is not documented.",color="red")
                        sys.exit(1)
                    elif self.argsOptions[truekey]['meta']\
                        and not args[key]and not self.argsOptions[truekey]['default'] and severity>=1:
                        display("Error, the command line option "+key+" expects "
                                        "the argument "+self.argsOptions[truekey]['meta'],
                                color="red")
                        sys.exit(1)
                    elif args[key] and not self.argsOptions[truekey]['meta']:
                        display("Error, the command line option "+key+" does not expect"
                                " the argument '" +" ".join(value) +"'.",color="red")
                        sys.exit(1)
        for option_name, option_param in self.args.items():
            if 'default' in option_param and not option_param['default'] is None:
                args[option_name] = args.getdefault(option_name, option_param['default'])
        for option_name, option_param in self.argsOptions.items():
            if 'default' in option_param and not option_param['default'] is None:
                args[option_name] = args.getdefault(option_name, option_param['default'])

        return args

def parseSysArgs():
    return parseArgsToDict(sys.argv[1:])
