import subprocess
from collections.abc import Iterable

import numpy as np
import os
import psutil
from time import time as clock
import sys
from time import sleep as sleep
import re
from utils import display


import matplotlib as mpl

old_rcparams = mpl.rcParams.copy()

def set_rcparams():
    mpl.rcParams['lines.linewidth'] = 2
    mpl.rcParams['axes.linewidth'] = 1.5
    mpl.rcParams['axes.labelsize'] = 16
    mpl.rcParams['xtick.labelsize'] = 16
    mpl.rcParams['ytick.labelsize'] = 16
    mpl.rcParams['legend.fontsize'] = 15
        
def set_labelsize(size=16):
    mpl.rcParams['axes.labelsize'] = size
    mpl.rcParams['xtick.labelsize'] = size
    mpl.rcParams['ytick.labelsize'] = size
    mpl.rcParams['font.size'] = size

set_rcparams()

def revert_rcparams():
    mpl.rcParams.update(old_rcparams)

import matplotlib.pyplot as plt

class Savefig:
    def __init__(self):
        self.__density = 150

    @property
    def density(self):
        return self.__density

    @density.setter
    def density(self, dpi):
        self.__density = int(dpi)

    def __call__(self, fileName, density=None, sizefactor=1.1, **kwargs):
        fig = kwargs.pop('fig', plt.gcf())
        level = kwargs.pop('level', 0)
        if density is None:
            density = self.__density
        if fileName.endswith('.png'):
            kwargs.update({'dpi': density})
            kwargs.update({'bbox_inches': 'tight'})
            # kwargs.update({'pad_inches':0.0});
        elif fileName.endswith(('.eps', '.pdf')):
            kwargs.update({'bbox_inches': 'tight','pad_inches':0})
            kwargs.pop('density', None)
            kwargs.pop('dpi', None) 
            if kwargs.get('trim',False):
                plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, 
                            hspace = 0, wspace = 0)
                plt.margins(0,0)
                plt.gca().xaxis.set_major_locator(plt.NullLocator())
                plt.gca().yaxis.set_major_locator(plt.NullLocator())
        size = fig.get_size_inches()
        fig.set_size_inches(size*sizefactor)
        fig.savefig(fileName, **kwargs)
        display("Saved "+fileName, level=level)


savefig = Savefig()

