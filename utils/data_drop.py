from collections.abc import Iterable
import os 

class DataDrop(dict):
    """
    A wrapper saving a bunch of datas in a folder.
    Usage:
    Assume a folder .tmp contains the files
        .tmp/f.gp
        .tmp/H.gp

    dataDrop=DataDrop(".tmp");#Read all data files in the folder .tmp 
    f=dataDrop['f.gp'];
    H=dataDrop['H.gp'];

    f.append(1);
    dataDrop.save();#save all files
    """

    def __init__(self, folder, ext=None, startEmpty=False):
        dict.__init__(self)
        self.folder = folder
        self.ext = ext
        if not startEmpty:
            self.retrieveData()

    def save(self):
        """Save data in a .gp file. 
        data can be a list of floats or a list of list of floats."""
        for (key, val) in self.items():
            with open(self.folder+"/"+key, "w") as f:
                try:
                    if isinstance(val[0], str):
                        f.write("String\n")
                        f.write("\n".join(line for line in val))
                    elif isinstance(val[0], Iterable):
                        f.write("Vector\n")
                        f.write("\n".join([" ".join([str(x)
                                                     for x in line]) for line in val]))
                    else:
                        f.write("Scalar\n")
                        f.write("\n".join([str(x) for x in val]))
                except:
                    f.write("Scalar\n")
                    pass
                f.write('\n')

    def retrieveData(self):
        """Read a data .gp file saved by saveData."""
        try:
            files = os.listdir(self.folder)
            if self.ext:
                files = [f for f in files if f.endswith(self.ext)]
        except:
            return
        for fileName in files:
            try:
                with open(self.folder+"/"+fileName, "r") as f:
                    lines = f.readlines()
                    if lines[0].strip() == 'Scalar':
                        data = list(map(float, lines[1:]))
                    elif lines[0].strip() == 'Vector':
                        try:
                            data = [[float(x) for x in line.split()]
                                    for line in lines[1:]]
                        except:
                            data = [[x for x in line.split()]
                                    for line in lines[1:]]
                    elif lines[0].strip() == 'String':
                        data = [f.strip() for f in lines[1:]]
                    else:
                        data = list(map(float, lines))
                self[fileName] = data
            except:
                pass

    def __getitem__(self, key) -> list:
        if not key in self.keys():
            self[key] = []
        return dict.__getitem__(self, key)

