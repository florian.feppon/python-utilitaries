from math import floor, log10   
import subprocess
import os
import time
import inspect
try:
    import colored as col

    def colored(text, color=None, attr=None):
        if color:
            text = col.stylize(text, col.fg(color))
        if attr:
            text = col.stylize(text, col.attr(attr))
        return text
except:
    def colored(text, color=None, attr=None):
        return text

tclock = dict()


def tic(ref=0):
    global tclock
    tclock[ref] = time.time()


def toc(ref=0):
    global tclock
    return format(time.time()-tclock[ref], "0.2f")+"s"


class cd:
    """Context manager for changing the current working directory"""

    def __init__(self, newPath, debug=1):
        self.newPath = os.path.abspath(os.path.expanduser(newPath))
        self.debug = debug

    def __enter__(self):
        if self.debug:
            print(f"Entering {self.newPath}.")
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


class Display:
    """Text printing function and save in the log file self.log_file.

    Parameter:
        message  : the text to print
        level    : (default 0). The importance of the message (0: maximum). The message 
                   is displayed if level<=self.debug
    """

    def __init__(self, log_file=None, debug=0, log_filter=None,
                 level_filter=None, process_log=None, print_cond=None,
                 process_print=None):
        self.log_file = log_file
        self.debug = debug
        if log_filter:
            self.log_filter = log_filter
        else:
            self.log_filter = lambda flag: True
        if level_filter:
            self.level_filter = level_filter
        else:
            self.level_filter = lambda x: x
        if process_log:
            self.process_log = process_log
        else:
            self.process_log = lambda x, flag: x
        if print_cond:
            self.print_cond = print_cond
        else:
            self.print_cond = lambda level, debug, flag: debug >= level
        if process_print:
            self.process_print = process_print
        else:
            self.process_print = lambda x, flag: x

    def __call__(self, message, level=0, debug=-1, color=None, attr=None, end='\n', flag=None):
        if color or attr:
            message = colored(message, color, attr)
        level = self.level_filter(level)
        if self.print_cond(level, debug, flag) or self.print_cond(level, self.debug, flag):
            print(self.process_print(message, flag), end=end, flush=True)
        if self.log_file and self.log_filter(flag):
            message = message.replace(r"\\", r"\\\\")
            message = message.replace(r"%", r"%%")
            message = self.process_log(message, flag)
            cmd = "printf \""+message+end+"\""
            subprocess.call(cmd+" >> "+self.log_file, shell=True)


display = Display()
def get_root(): 
    file = inspect.stack()[1][1]
    return os.path.dirname(file)

def indexr(s: str, key: str):
    index = s.index(key)
    return index+len(key)
    
    
# Define function for string formatting of scientific notation
def sci_notation(num, decimal_digits=1, precision=None, exponent=None):
    """
    Returns a string representation of the scientific
    notation of the given number formatted for use with
    LaTeX or Mathtext, with specified number of significant
    decimal digits and precision (number of decimal digits
    to show). The exponent to be used can also be specified
    explicitly.
    """
    if exponent is None:
        if num == 0:
            return "0"
        else:
            exponent = int(floor(log10(abs(num))))
    coeff = round(num / float(10**exponent), decimal_digits)
    if precision is None:
        precision = decimal_digits

    return r"${0:.{2}f}\cdot10^{{{1:d}}}$".format(coeff, exponent, precision)
