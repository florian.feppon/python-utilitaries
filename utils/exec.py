import subprocess
from time import time as clock
import psutil
import threading
import queue

from utils import Display, colored, display, tic, toc


def enqueue_stream(stream, queue, type):
    for line in iter(stream.readline, b''):
        queue.put(str(type) + line.decode('utf-8', errors='replace'))
    stream.close()


def enqueue_process(process, queue):
    process.wait()
    queue.put('x')


class Exec1:
    """Blocking exec. Do not use this exec if you want to capture standard output.
    Use it to run interactive shell programs such as less or vim or medit."""

    def __init__(self, debug=0, log_file=None, stdout=None, level=0,
                 display=None):
        self.debug = debug
        self.level = level
        self.log_file = log_file
        self.stdout = stdout
        if display is None:
            self.display = Display(log_file, debug)
        else:
            self.display = display

    def __call__(self, cmd, getStdout=False, level=None, end=''):
        """Help"""
        if level is None:
            level = self.level
        self.display(colored(cmd.split()[0], color=149)+" "
                     + colored(" ".join(cmd.split()[1:]), color=150), end=end, level=level)
        if not getStdout:
            if self.stdout:
                cmd = cmd+" >> "+self.stdout + " 2>&1"
            tclock = clock()
            child = subprocess.Popen(cmd, shell=True)
            p = psutil.Process(child.pid)
            p.wait()
            self.display(" ("+format(clock()-tclock, '.02f')+")", attr="dim",
                         level=level)
        else:
            tclock = clock()
            res = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
            p = psutil.Process(res.pid)
            p.wait()
            self.display(" ("+format(clock()-tclock, '.02f')+")",
                         attr="dim", level=level)
            return ''.join([x.decode('utf-8', errors='replace') for x in res.stdout.readlines()])


class ExecException(Exception):
    def __init__(self, message, returncode, stdout, stderr, mix):
        super().__init__(message)
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        self.mix = mix


exec1 = Exec1(debug=2)


def exec2(cmd, **kwargs):
    """ Interface with subprocess.Popen """
    debug = kwargs.pop('debug', 0)
    level = kwargs.pop('level', 1)
    silent = kwargs.pop('silent', True)
    display(colored(cmd, color="yellow"), level=level, debug=debug, end='',
            flag='shell')
    # Put a line break to separate from the stdout
    if not silent:
        display("", level=level, debug=debug, flag='')
    tic(121)
    proc = subprocess.Popen("stdbuf -oL "+cmd, shell=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE
                            )
    q = queue.Queue()
    to = threading.Thread(target=enqueue_stream, args=(proc.stdout, q, 1))
    te = threading.Thread(target=enqueue_stream, args=(proc.stderr, q, 2))
    tp = threading.Thread(target=enqueue_process, args=(proc, q))
    te.start()
    to.start()
    tp.start()

    stdout = ""
    stderr = ""
    mix = ""
    while True:
        line = q.get()
        if line[0] == 'x':
            break
        if line[0] == '1':
            line = line[1:]
            stdout += line
            mix += line
            if not silent:
                display(line, level=level+1, debug=debug,
                        end='', flag='stdout')
        if line[0] == '2':
            line = line[1:]
            stderr += line
            mix += line
            if not silent:
                display(line, level=level+1, debug=debug,
                        attr="dim", end='', flag='stderr')
    tp.join()
    te.join()
    to.join()
    if mix and not silent:
        if not mix.endswith('\n'):
            display("", level, debug)
        display("Finished in", level, debug, color="cyan", end="", flag="")
    display(' ('+toc(121)+')', level=level, debug=debug, color="cyan",
            flag="time")

    if proc.returncode != 0:
        raise ExecException('Error : the process "'
                            + colored(cmd, "red")
                            + '" failed with return code '+str(proc.returncode)
                            + ".", proc.returncode, stdout, stderr, mix)
    return proc.returncode, stdout, stderr, mix
