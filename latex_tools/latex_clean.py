# Utilitary to clean latex files
# Create a folder "clean" with required source files and images in it
# Files are renamed to be on a single place
from shutil import copyfile
import regex as re
from utils import exec1, cd, argDocString
import os

argParser = argDocString('Clean a latex repository by putting all figures '
                         'and macro files in a folder "clean/".')
argParser.addArg(0, 'Tex file to process','file.tex')
argParser.addArg('-u', 'If present, then creates a file '
                 'file_to_import_imported.tex'
                 ' by reverting file paths (assuming it is a modification '
                 ' of a "cleaned" version of file.tex). Useful to process '
                 'corrections that have been made in  the cleaned folder','file_to_import.tex')
argParser.addArg('--skip_compile', 'Skip the compilation at the end of the '
                 'export')
argParser.addArg('--skip_erase', 'Do not erase the clean directory.')

args=argParser.parseSysArgs()

filename = args[0]
filename = os.path.splitext(filename)[0]

with open(f"{filename}.tex") as f:
    lines = f.read()

if '-u' in args:
    with open(f"{args['-u'][0]}") as f:
        linesImport = f.read()

# Removing commented lines  
lines = '\n'.join([line for line in lines.splitlines() if not line.startswith('%')])
# files=re.findall(r'[a-zA-Z0-9_\/\.]+[\.\/][a-z]+',lines);
files = re.findall(r'{[a-zA-Z0-9_\/\.\-]+}', lines)
files = [x[1:-1] for x in files]
filesbib = list(*re.findall(r'bibliography{([a-zA-Z0-9_\/\.]+),*([a-zA-Z0-9_\/\.]+)*}',lines))
files += [f for f in filesbib if not f in files]
#[filesbib[0]] + [f[1:] for f in filesbib[1:] if not f in files]

beamercolorthemes = re.findall(r'\\usecolortheme{([a-zA-Z0-9_\/\.]+)}',lines)
files += ['beamercolortheme'+name+'.sty' for name in beamercolorthemes]

if not '-u' in args and not '--skip_erase' in args:
    exec1("rm -rf clean/")
    exec1("mkdir clean")
filesAdd = dict()
namesConvert = dict()


def bracket(s):
    return "{"+s+"}"


for k, f in enumerate(files):
    continu = False
    exts = dict()
    copy = False
    if os.path.exists(f) and not f=='.':
        filePath = f
        continu = True
        copy = True
        exts[f] = ''
        ext = os.path.splitext(filePath)[1]
        #if ext=='.tex':
        #    exec1(f'python -m latex_tools.latex_clean {filePath} --skip_erase --skip_compile')
        #    copy = False
    for ext in ['.pdf', '.eps',  '.sty', '.cls', '.bib', '.bst']:
        if os.path.exists(f+ext):
            copy = True
            filePath = f+ext
            exts[f] = ext
            continu = True
    if continu:
        (baseName, ext) = os.path.splitext(os.path.basename(f))
        if baseName in filesAdd:
            outName = baseName+'_'+str(filesAdd[baseName])\
                + ext
            filesAdd[baseName] += 1
        else:
            outName = baseName+ext
            filesAdd[baseName] = 0
        if filePath != f:
            outPath = outName+exts[f]
        else:
            outPath = outName

        if not '-u' in args:
            print(f"{filePath} =====> clean/{outPath}")
            if filePath==".":
                import ipdb 
                ipdb.set_trace()
            if copy:
                copyfile(filePath, "clean/"+outPath)
            lines = re.sub(re.escape(f), outName, lines)
        else:
            namesConvert[f] = outName
            print(f"{outName}==>{f}")
            linesImport = re.sub(
                r"([^_^])"+re.escape(bracket(outName)), r'\1'+bracket(f), linesImport)


if not '-u' in args:
    filename=os.path.basename(filename)
    with open(f"clean/{filename}.tex", "w") as f:
        f.write(lines)
    with cd("clean"):
        if not '--skip_compile' in args:
            exec1(f"python -m latex_tools.pdflatex {filename}.tex")
else:
    with open(f"{os.path.splitext(args['-u'][0])[0]}_imported.tex", "w") as f:
        f.write(linesImport)
        print(f"Written {os.path.splitext(args['-u'][0])[0]}_imported.tex.")
