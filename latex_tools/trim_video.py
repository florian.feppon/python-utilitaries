from utils import exec1, cd
from utils import argDocString

import os

argParser = argDocString(
    "Convert a video into the mp4 or webm format using ffmpeg")
argParser.addArg(0, "Input files (can be several files)", "FILE1 FILE2...")
argParser.addArg(
    '-trim', 'If this option is present, white margins will be removed')
argParser.addArg('-crop', 'If this option is present, the video is cropped according to the window provided',
                 meta='640x480+50+100')
argParser.addArg('--output', 'Specify the format of the output video',
                 meta='webm|mp4', default='webm')
args = argParser.parseSysArgs()


exec1.debug = 0
for n in range(args.nargs):
    filename = os.path.splitext(args[n])[0]
    exec1('rm -rf .tmp')
    exec1('mkdir .tmp')
    if not "--skip_first_ffmpeg" in args:
        exec1(f'ffmpeg -i {args[n]} -r 24 -f image2 .tmp/{filename}-%3d.bmp')
    with cd('.tmp'):
        if "-trim" in args:
            exec1('mogrify -trim *.bmp')
        elif "-crop" in args:
            exec1('mogrify -crop '+" ".join(args["-crop"])+" *.bmp")
    if args["--output"] == "webm":
        exec1(
            f"ffmpeg -i .tmp/{filename}-%3d.bmp  -b:v 0 -pix_fmt yuv420p -vcodec libvpx-vp9 -crf 30 -b:v 0 -r 12 -b:a 128k -c:a libopus {filename}.new.webm")
        exec1(f"convert .tmp/{filename}-001.bmp {filename}.png")
    else:
        exec1(
            f'ffmpeg  -i .tmp/{filename}-%3d.bmp -f mp4 -pix_fmt yuv420p -q:v 0 -vcodec mpeg4   {filename}.new.mp4')
        exec1(f"convert .tmp/{filename}-001.bmp {filename}.png")
