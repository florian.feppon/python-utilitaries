from utils import argDocString, exec1 
import os

argParser = argDocString("Quick compilation of a latex file with pdflatex and bibtex.")
argParser.addArg(0, "Main latex file", "FILE.tex")

args = argParser.parseSysArgs()

texfile = args[0]


# First step : pass into draft mode
cmd1 = r"sed  -i -r 's/^(\\documentclass\[)(draft,)([a-zA-Z0-9,]*)\]/\1\3\]/g' "
cmd2 = r"sed  -i -r 's/^(\\documentclass\[)([a-zA-Z0-9,]*)(,draft)([a-zA-Z0-9,]*)\]/\1\2\4\]/g' "
exec1(f"{cmd1} {texfile}")
exec1(f"{cmd2} {texfile}")

cmd = r"sed  -i -r 's/^(\\documentclass\[)/\1draft,/g' "
exec1(f"{cmd} {texfile}")

# Compile + Bib
prefix = os.path.splitext(texfile)[0]
exec1(f"pdflatex -shell-escape {prefix}.tex")

if os.path.isfile(f'{prefix}.nlo'):
    exec1(f"makeindex {prefix}.nlo -s nomencl.ist -o {prefix}.nls -t {prefix}.nlg")

exec1(f"bibtex {prefix}.aux")
exec1(f"pdflatex -shell-escape {prefix}.tex")

# Revert images
cmd = r"sed -i -r 's/^(\\documentclass\[)draft,/\1/g'"
exec1(f"{cmd} {texfile}")

# Final compilation
exec1(f"pdflatex -shell-escape {prefix}.tex")
