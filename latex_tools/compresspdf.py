from utils import exec2, cd

from utils import argDocString
import sys
import os

argParser = argDocString("Compress a pdf with ghostscript.")
argParser.addArg(0, 'PDF file to compress with ghostscript.', 'file.pdf', important=True)
argParser.addArg('--quality','Quality of the compression. Q must be chosen between '
                 '(screen|ebook| prepress | printer)', 'Q', default='ebook')

args = argParser.parseSysArgs()

filename = os.path.splitext(args[0])[0]

exec2(f"gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/"
      + args['--quality'] +
      f" -dNOPAUSE -dQUIET -dBATCH -dPrinted=false"
      f" -sOutputFile={filename}_small.pdf {filename}.pdf")
