# Python-utilitaries

In this package, you will find a bunch of useful tools:

In the folder `utils`:

- `exec1` and `exec2`: run shell commands from python and retrieve the standard
  output
- `display` print and save into a log file according to some verbosity level
- `argDocString` : a class to easily parse the shell inputs with custom arguments (a
  bit like `argParse`)
- `DataDrop`: a class for saving dictionary of list of numbers into files
- `savefig`: a tool to save pyplot figures 

In the folder `latex-tools`, a bunch of python utilitaries for latex editing.
Documentation of these tools can be seen by calling them in command line 
with the `-h` option. 

```bash
python compresspdf.py -h
python latex_clean.py -h
python trim_video.py -h
```

